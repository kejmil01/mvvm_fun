package com.example.dell.mvvmtest;


import java.util.List;

public interface MainViewModelContract {
    interface View{
        void onDataListChanged(List<String> textList);

        void clearDataList();
    }
}
