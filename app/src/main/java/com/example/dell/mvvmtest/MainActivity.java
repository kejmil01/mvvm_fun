package com.example.dell.mvvmtest;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.example.dell.mvvmtest.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainViewModelContract.View {

    private ActivityMainBinding mMainBinding;
    private MainViewModel mMainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mMainViewModel = new MainViewModel(this, new SearchMicroService());
        mMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mMainBinding.setViewModel(mMainViewModel);

        setupRecyclerView();
    }

    private void setupRecyclerView() {
        mMainBinding.recycler.setLayoutManager(new LinearLayoutManager(this));
        mMainBinding.recycler.setAdapter(new MyAdapter());
    }

    @Override
    public void onDataListChanged(List<String> textList) {
        MyAdapter myAdapter = (MyAdapter) mMainBinding.recycler.getAdapter();
        myAdapter.setStringList(textList);
        myAdapter.notifyDataSetChanged();
    }

    @Override
    public void clearDataList() {
        MyAdapter myAdapter = (MyAdapter) mMainBinding.recycler.getAdapter();
        myAdapter.setStringList(new ArrayList<String>());
        myAdapter.notifyDataSetChanged();
    }
}
