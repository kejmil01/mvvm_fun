package com.example.dell.mvvmtest;

import android.databinding.Observable;
import android.databinding.ObservableBoolean;

import com.example.dell.mvvmtest.binding.ObservableString;

import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.subjects.PublishSubject;

public class MainViewModel {

    public ObservableString text = new ObservableString();
    public ObservableBoolean searchingInProgress = new ObservableBoolean(false);

    private PublishSubject<String> textSubject = PublishSubject.create();

    private MainViewModelContract.View mMainView;
    private SearchMicroService mSearchMicroService;

    MainViewModel(MainViewModelContract.View mainView,
                  SearchMicroService searchMicroService) {
        mMainView = mainView;
        mSearchMicroService = searchMicroService;
        initialize();
    }

    private void initialize() {
        text.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                ObservableString observableString = (ObservableString) sender;
                textSubject.onNext(observableString.get());
            }
        });

        setupSearching();
    }

    private void setupSearching() {
        buildTextObservable()
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        searchingInProgress.set(true);

                        mMainView.clearDataList();
                        mSearchMicroService.buildSearchObservable(s)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Subscriber<List<String>>() {
                                    @Override
                                    public void onCompleted() {
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        searchingInProgress.set(false);
                                    }

                                    @Override
                                    public void onNext(List<String> strings) {
                                        searchingInProgress.set(false);
                                        mMainView.onDataListChanged(strings);
                                    }
                                });
                    }
                });
    }

    private rx.Observable<String> buildTextObservable() {
        return textSubject.asObservable()
                .debounce(1000, TimeUnit.MILLISECONDS)
                .filter(new Func1<String, Boolean>() {
                    @Override
                    public Boolean call(String s) {
                        return !s.isEmpty();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread());
    }
}
