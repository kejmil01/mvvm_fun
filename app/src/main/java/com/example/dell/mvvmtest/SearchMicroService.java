package com.example.dell.mvvmtest;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.functions.Func1;

public class SearchMicroService {
    public Observable<List<String>> buildSearchObservable(final String searchFraze) {
        return Observable.timer(3000, TimeUnit.MILLISECONDS)
                .map(new Func1<Long, List<String>>() {
                    @Override
                    public List<String> call(Long aLong) {
                        List<String> stringList = new ArrayList<>();
                        for (int i = 0; i < 15; i++) {
                            String text = String.valueOf(i) + "_ " + searchFraze;
                            stringList.add(text);
                        }
                        return stringList;
                    }
                })
                .first();
    }
}
