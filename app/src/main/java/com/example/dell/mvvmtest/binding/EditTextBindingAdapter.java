package com.example.dell.mvvmtest.binding;


import android.databinding.BindingAdapter;
import android.databinding.BindingMethod;
import android.databinding.BindingMethods;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

import com.example.dell.mvvmtest.R;

@BindingMethods({
        @BindingMethod(type = TextView.class,
                attribute = "android:onEditorAction",
                method = "setOnEditorActionListener"),
})
public class EditTextBindingAdapter {
    @BindingAdapter({"android:text"})
    public static void bindEditText(TextView view, final ObservableString observableString) {
        if (view.getTag(R.id.binded) == null) {
            view.setTag(R.id.binded, true);
            view.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    observableString.set(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }
        String newValue = observableString.get();
        if (!view.getText().toString().equals(newValue)) {
            view.setText(newValue);
        }
    }
}
